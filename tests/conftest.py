# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""Set session level fixtures."""

from os.path import dirname, join, pardir

import cobra
import pytest

from dfba import DfbaModel, ExchangeFlux, KineticVariable


config = cobra.Configuration()
config.solver = "glpk"


@pytest.fixture(scope="session")
def iJR904():
    """Load from cobrapy."""
    model = cobra.io.read_sbml_model(
        join(dirname(__file__), pardir, "sbml-models", "iJR904.xml.gz")
    )
    return model


@pytest.fixture(scope="function")
def dfba_model(iJR904):
    """Model that will be used in (1) kinetic and (2) parameters.

    This instantiation is also performed as a separate independent test.

    """
    return DfbaModel(iJR904.copy())


@pytest.fixture(scope="session")
def kinetic_variables():
    """Provide a set KineticVariables."""
    x = KineticVariable("Biomass")
    gluc = KineticVariable("Glucose")
    xyl = KineticVariable("Xylose")
    oxy = KineticVariable("Oxygen")
    eth = KineticVariable("Ethanol")
    return [x, gluc, xyl, oxy, eth]


@pytest.fixture(scope="session")
def exchange_reactions():
    """Provide a set of ExchangeFluxes."""
    mu = ExchangeFlux("BiomassEcoli")
    v_g = ExchangeFlux("EX_glc(e)")
    v_z = ExchangeFlux("EX_xyl_D(e)")
    v_o = ExchangeFlux("EX_o2(e)")
    v_e = ExchangeFlux("EX_etoh(e)")
    return [mu, v_g, v_z, v_o, v_e]


@pytest.fixture(scope="session")
def initials():
    """Provide a set of conditions."""
    return dict(Biomass=0.03, Glucose=15.5, Xylose=8.0, Oxygen=0.24, Ethanol=0.0)


@pytest.fixture(scope="function")
def assembled_dfba(dfba_model, kinetic_variables, exchange_reactions, initials):
    """Provide model ready to be simulated.

    This is the same as in `examples/example1` and it is separated into some
    different tests at test_kinetic.py.

    """
    dfba_model.add_kinetic_variables(kinetic_variables)
    dfba_model.add_exchange_fluxes(exchange_reactions)

    dfba_model.add_rhs_expression(
        "Biomass", kinetic_variables[0] * kinetic_variables[0]
    )
    dfba_model.add_rhs_expression(
        "Glucose",
        kinetic_variables[1] * 180.1559 * kinetic_variables[0] / 1000.0,
    )
    dfba_model.add_rhs_expression(
        "Xylose", kinetic_variables[2] * 150.13 * kinetic_variables[0] / 1000.0
    )
    dfba_model.add_rhs_expression(
        "Oxygen", kinetic_variables[3] * 16.0 * kinetic_variables[0] / 1000.0
    )
    dfba_model.add_rhs_expression(
        "Ethanol",
        kinetic_variables[4] * 46.06844 * kinetic_variables[0] / 1000.0,
    )

    dfba_model.add_exchange_flux_lb(
        "EX_glc(e)",
        10.5
        * (kinetic_variables[1] / (0.0027 + kinetic_variables[1]))
        * (1 / (1 + kinetic_variables[1] / 20.0)),
        kinetic_variables[1],
    )
    dfba_model.add_exchange_flux_lb(
        "EX_o2(e)",
        15.0 * (kinetic_variables[3] / (0.024 + kinetic_variables[3])),
        kinetic_variables[3],
    )
    dfba_model.add_exchange_flux_lb(
        "EX_xyl_D(e)",
        6.0
        * (kinetic_variables[2] / (0.0165 + kinetic_variables[2]))
        * (1 / (1 + kinetic_variables[4] / 20.0))
        * (1 / (1 + kinetic_variables[1] / 0.005)),
        kinetic_variables[2],
    )

    dfba_model.add_initial_conditions(initials)
    return dfba_model

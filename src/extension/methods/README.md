## *METHODS* source files

This directory contains the following content

* [`./methods_direct.cpp`](./methods_direct.cpp): contains source code for model integration
  using direct method
* [`./methods_harwood.cpp`](./methods_harwood.cpp): contains source code for model integration
  using Harwood et al. algorithm
* [`./methods.h`](./methods.h): contains *SUNDIALS* includes and declarations
  for integration methods
* [`./methods.cpp`](./methods.cpp): contains functions used by methods
* [`./methods_scott.cpp`](./methods_scott.cpp): contains source code for model integration using
  Scott et al. algorithm
* [`./README.md`](./README.md): this document


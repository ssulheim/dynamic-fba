## *EMBLP* source files

This directory contains the following content

* [`./emblp_direct.cpp`](./emblp_direct.cpp): contains member functions for
  direct method derived class
* [`./emblp_harwood.cpp`](./emblp_harwood.cpp): contains member functions for
  Harwood et al. derived class
* [`./emblp.cpp`](./emblp.cpp): contains member functions for embedded
  LP problem base class
* [`./emblp_scott.h`](./emblp_scott.h): contains member functions for Scott et
  al. derived class
* [`./emblp.h`](./emblp.h): contains class declarations for embedded LP problems
* [`./README.md`](./README.md): this document


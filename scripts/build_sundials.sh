#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -eux

# Use a default of one core.
: "${NPROC:=1}"

build_dir=sundials-build

# Download the specified version.
curl -L -O "https://computation.llnl.gov/projects/sundials/download/sundials-${SUNDIALS_VERSION}.tar.gz"

# Unpack and install SUNDIALS.
tar -xzf "sundials-${SUNDIALS_VERSION}.tar.gz"
mkdir "${build_dir}"
cd "${build_dir}"
cmake \
    -DBUILD_STATIC_LIBS=OFF \
    -DEXAMPLES_INSTALL=OFF \
    "../sundials-${SUNDIALS_VERSION}"
make -j "${NPROC}" install
cd ..
rm -rf "sundials-${SUNDIALS_VERSION}"* "${build_dir}"

---
title: 'dfba: Software for efficient simulation of dynamic flux-balance analysis models in Python'
tags:
 - Mathematical optimization
 - Linear programming
 - Numerical simulation
 - Multi-scale metabolic modeling
 - C++
 - Python
authors:
 - name: David S. Tourigny
   orcid: 0000-0002-3987-8078
   affiliation: 1
 - name: Jorge Carrasco Muriel
   orcid: 0000-0001-7365-0299
   affiliation: 2
 - name: Moritz E. Beber
   orcid: 0000-0003-2406-1978
   affiliation: 2
affiliations:
 - name: Columbia University Irving Medical Center, 630 West 168th Street, New York, NY 10032 USA
   index: 1
 - name: Novo Nordisk Foundation Center for Biosustainability, Technical University of Denmark, Building 220, Kemitorvet, 2800 Kongens Lyngby, Denmark
   index: 2
date: 13 March 2020
bibliography: paper.bib
---

# Summary

Flux-balance analysis (FBA) is a computational method based on linear programming (LP) that has had enormous success modeling the metabolic behaviour of organisms and cellular systems existing in steady state with their environment [@Varma94; @Orth10]. Representing the corresponding biological model as an LP problem means that FBA can be used to study metabolism at genome-scale. Unfortunately, the underlying assumption of an unchanging environment means that FBA is not immediately applicable to systems with dynamics where, for example, environmental conditions may vary in time. Extensions of FBA, including dynamic FBA (DFBA) [@Mahadevan02], have therefore been developed in order to accommodate temporal dynamics into the framework of genome-scale metabolic modeling.

Although DFBA is well-defined mathematically as an LP problem embedded in a system of ordinary differential equations (ODEs), numerical simulation of DFBA models proves particularly challenging (as described in @Harwood16). Consequently, @Harwood16 proposed an algorithm for efficiently simulating DFBA models based on reformulating the ODE system as a differential algebraic equation (DAE) with root detection and representing solutions of the LP problem using an optimal basis formulation. An initial implementation of this algorithm has been provided in the software package DFBAlab [@Gomez14] written in MATLAB and using commercial LP solvers.

Increasingly, researchers engaged in metabolic modeling prefer open source software. Python is quickly becoming their platform of choice [@Carey20]. Among other packages, open source resources for building and simulating FBA models using Python can be found in the COBRApy [@Ebrahim13] package which is part of the openCOBRA organization [@opencobra]. Until now, COBRApy lacked an efficient implementation of DFBA using the DAE formulation.

## Statement of need:
_Researchers wanting to build and simulate specific models of interest often lack the background in numerical analysis or high-performance computing required to overcome the numerical challenges of DFBA_.

We have solved this issue by developing a software package based on open source libraries GLPK [@glpk] and SUNDIALS [@Hindmarsh05] that implements the most efficient algorithms in a compiled programming language that is made accessible to users through a simple and intuitive pybind11 [@pybind11] interface with pandas [@McKinney11] and the openCOBRA Python module COBRApy [@Ebrahim13]. ODEs are constructed using the symbolic expression enabled by optlang [@Jensen16], SymPy, and SymEngine [@Meurer17].

# Acknowledgements

DST is a Simons Foundation Fellow of the Life Sciences Research Foundation. MEB
received funding from the European Union’s Horizon 2020 research and innovation
programme under grant agreement 686070 (DD-DeCaF). We thank Peter St. John and Christian Diener for discussions and suggestions.

# References

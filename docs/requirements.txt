nbsphinx>=0.5.0
sphinx-autoapi>=1.2.1
sphinx-material
sphinx-copybutton>=0.2.6
ipykernel

# check https://github.com/sphinx-doc/sphinx/issues/8198
pygments>=2.4.1


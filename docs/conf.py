"""Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
http://www.sphinx-doc.org/en/master/config

-- Path setup --------------------------------------------------------------

If extensions (or modules to document with autodoc) are in another directory,
add these directories to sys.path here. If the directory is relative to the
documentation root, use os.path.abspath to make it absolute, like shown here.

"""
# import os
# import sys
import sphinx_material

# sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------

project = "dfba"
copyright = """
2018,2019 Columbia University Irving Medical Center, New York, USA.
2019 Novo Nordisk Foundation Center for Biosustainability
"""
author = "David S. Tourigny, Moritz E. Beber"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx.ext.autosummary",
    "autoapi.extension",
    "nbsphinx",
    "sphinx_material",
    "sphinx_copybutton",
]

# Document Python Code
autoapi_dirs = ["../src/dfba"]

# Napoleon settings
napoleon_numpy_docstring = True

# The master toctree document.
master_doc = "index"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".ipynb_checkpoints"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# Choose the material theme
html_theme = "sphinx_material"
# Get the them path
html_theme_path = sphinx_material.html_theme_path()
# Register the required helpers for the html context
html_context = sphinx_material.get_html_context()
html_sidebars = {
    "**": [
        "logo-text.html",
        "globaltoc.html",
        "localtoc.html",
        "searchbox.html",
    ]
}

html_theme_options = {
    # Set the name of the project to appear in the navigation.
    "nav_title": "dfba",
    "repo_url": "https://gitlab.com/davidtourigny/dynamic-fba/",
    "repo_name": "dfba",
    "repo_type": "gitlab",
    "html_minify": False,
    "html_prettify": False,
    "css_minify": True,
    "globaltoc_depth": 2,
    "color_primary": "green",
    "color_accent": "light-green",
    "theme_color": "#2196f3",
    "master_doc": False,
    "heroes": {
        "index": "A flexible modeling package for dynamic flux-balance "
        "analysis simulations.",
        "installation": "Steps to take in order to build and install dfba.",
        "plotting": "Additional dependencies for visualizing simulations.",
        "examples": "Information on the GEM models used in tutorials and " "examples.",
        "example1": "Tutorial 1/3: Building and simulating a DFBA model",
        "example3": "Tutorial 2/3: Using control parameters",
        "example5": "Tutorial 3/3: Interface to the solver",
        "contributing": "Developer's Guide",
        "source_files": "Developer's Guide",
    },
}
# html_theme_options = {
# #     #'logo': 'some_logo.png',
# #     'sidebar_includehidden': True,
# #     'fixed_sidebar': True,
#     # 'gitlab_user': 'davidtourigny',
#     # 'gitlab_repo': 'dynamic-fba',
#     # 'gitlab_banner': True,
# #     'github_button': False,
# }

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ["_static"]

intersphinx_mapping = {
    "http://docs.python.org/": None,
    "http://docs.scipy.org/doc/numpy/": None,
    "http://docs.scipy.org/doc/scipy/reference": None,
}
